import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Pizza, PizzaService} from '../pizza.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastService} from '../../toast/toast.service';

@Component({
  selector: 'app-pizza-editing',
  templateUrl: './pizza-editing.component.html',
  styleUrls: ['./pizza-editing.component.scss']
})
export class PizzaEditingComponent implements OnInit {

  id: number;
  pizzaFormGroup: FormGroup;

  constructor(private route: ActivatedRoute,
              private pizzaService: PizzaService,
              private formBuilder: FormBuilder,
              private router: Router,
              public toastService: ToastService) {
  }

  ngOnInit(): void {
    this.createPizzaFormGroup();

    this.id = +this.route.snapshot.params.id;

    if (this.id) {
      this.pizzaService.getOne(this.id)
        .subscribe((returnedPizza: Pizza) => {
          this.setValuesInPizzaFormGroup(returnedPizza);
        });
    }
  }

  private createPizzaFormGroup(): void {
    this.pizzaFormGroup = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        size: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        price: ['', [Validators.required, Validators.min(0.1), Validators.pattern('^[0-9]+([\\.][0-9]{1,2})?$')]],
        description: ['', [Validators.required, Validators.minLength(3)]]
      },
      {updateOn: 'blur'}
    );
  }

  private setValuesInPizzaFormGroup(returnedPizza: Pizza): void {
    this.pizzaFormGroup.setValue({
      name: returnedPizza.name,
      size: returnedPizza.size,
      price: returnedPizza.price,
      description: returnedPizza.description
    });
  }

  get nameFormControl(): FormControl {
    return this.pizzaFormGroup.get('name') as FormControl;
  }

  get sizeFormControl(): FormControl {
    return this.pizzaFormGroup.get('size') as FormControl;
  }

  get priceFormControl(): FormControl {
    return this.pizzaFormGroup.get('price') as FormControl;
  }

  get descriptionFormControl(): FormControl {
    return this.pizzaFormGroup.get('description') as FormControl;
  }

  onSubmit(): void {
    if (this.pizzaFormGroup.valid) {
      this.pizzaEdited();
    } else {
      this.pizzaFormGroup.markAllAsTouched();
    }
  }

  private pizzaEdited(): void {
    this.pizzaService.edit(this.id, this.pizzaFormGroup.value)
      .subscribe((returnedPizza: Pizza) => {
        this.router.navigate(['../../'], {relativeTo: this.route})
          .then(() => {
            this.showEditedToast(returnedPizza);
          });
      });
  }

  private showEditedToast(returnedPizza: Pizza): void {
    this.toastService.show(
      'Pizza Edited',
      'Name: ' + returnedPizza.name + '<br/>' +
      'Size: ' + returnedPizza.size + '<br/>' +
      'Price: ' + returnedPizza.price + '<br/>' +
      'Description: ' + returnedPizza.description,
      {classname: 'bg-warning text-light'}
    );
  }

}
