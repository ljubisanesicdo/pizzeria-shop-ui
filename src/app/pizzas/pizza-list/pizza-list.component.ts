import {Component, OnInit} from '@angular/core';
import {Pizza, PizzaService} from '../pizza.service';
import {ToastService} from '../../toast/toast.service';

@Component({
  selector: 'app-pizza-list',
  templateUrl: './pizza-list.component.html',
  styleUrls: ['./pizza-list.component.scss']
})
export class PizzaListComponent implements OnInit {

  allPizzas: Pizza[];

  constructor(private pizzaService: PizzaService,
              private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.pizzaService.getAll()
      .subscribe((pizzas: Pizza[]) => {
        this.allPizzas = pizzas;
      });
  }

  onDelete(deletedPizza: Pizza): void {
    if (confirm('Do you want to delete pizza: ' + deletedPizza.name + ' ' + deletedPizza.size + '?')) {
      this.pizzaService.delete(deletedPizza.id)
        .subscribe(() => {
          this.showDeletedToast(deletedPizza);
          this.removePizzaFromArray(deletedPizza);
        });
    }
  }

  private removePizzaFromArray(deletedPizza: Pizza): void {
    const index = this.allPizzas.indexOf(deletedPizza);
    this.allPizzas.splice(index, 1);
  }

  private showDeletedToast(deletedPizza: Pizza): void {
    this.toastService.show(
      'Pizza Deleted',
      'Name: ' + deletedPizza.name + '<br/>' +
      'Size: ' + deletedPizza.size + '<br/>' +
      'Price: ' + deletedPizza.price + '<br/>' +
      'Description: ' + deletedPizza.description,
      {classname: 'bg-danger text-light'});
  }

}
