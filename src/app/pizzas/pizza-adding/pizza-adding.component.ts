import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AssignedGrocery, Pizza, PizzaService} from '../pizza.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastService} from '../../toast/toast.service';
import {Grocery} from '../../groceries/grocery.service';

@Component({
  selector: 'app-pizza-adding',
  templateUrl: './pizza-adding.component.html',
  styleUrls: ['./pizza-adding.component.scss']
})
export class PizzaAddingComponent implements OnInit {

  pizzaFormGroup: FormGroup;
  allGroceries: string[] = [];
  groceriesIsLoaded = false;
  groceriesForSelect: string[][] = [];
  lastChanged: string[] = [];
  deletedGroceryFormCounter = 0;

  constructor(private formBuilder: FormBuilder,
              private pizzaService: PizzaService,
              private router: Router,
              private route: ActivatedRoute,
              public toastService: ToastService) {
  }

  ngOnInit(): void {
    this.createPizzaFormGroup();
  }

  onSubmit(): void {
    if (this.pizzaFormGroup.valid) {
      const pizza = this.changeFormToPizza(this.pizzaFormGroup);
      // console.log(pizza);
      this.pizzaAdded(pizza);
    } else {
      this.pizzaFormGroup.markAllAsTouched();
    }
  }

  private createPizzaFormGroup(): void {
    this.pizzaFormGroup = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        size: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        price: ['', [Validators.required, Validators.min(0.1), Validators.pattern('^[0-9]+([\\.][0-9]{1,2})?$')]],
        description: ['', [Validators.required, Validators.minLength(3)]],
        assignedGroceries: this.formBuilder.array([], [Validators.required])
      },
      {updateOn: 'blur'}
    );
  }

  get nameFormControl(): FormControl {
    return this.pizzaFormGroup.get('name') as FormControl;
  }

  get sizeFormControl(): FormControl {
    return this.pizzaFormGroup.get('size') as FormControl;
  }

  get priceFormControl(): FormControl {
    return this.pizzaFormGroup.get('price') as FormControl;
  }

  get descriptionFormControl(): FormControl {
    return this.pizzaFormGroup.get('description') as FormControl;
  }

  get groceriesFormArray(): FormArray {
    return this.pizzaFormGroup.get('assignedGroceries') as FormArray;
  }

  private pizzaAdded(addedPizza: Pizza): void {
    this.pizzaService.add(addedPizza)
      .subscribe((returnedPizza: Pizza) => {
        this.router.navigate(['../'], {relativeTo: this.route})
          .then(() => {
            this.showAddedToast(returnedPizza);
          });
      });
  }

  private showAddedToast(returnedPizza: Pizza): void {
    this.toastService.show(
      'Pizza Added',
      'Name: ' + returnedPizza.name + '<br/>' +
      'Size: ' + returnedPizza.size + '<br/>' +
      'Price: ' + returnedPizza.price + '<br/>' +
      'Description: ' + returnedPizza.description,
      {classname: 'bg-success text-light'});
  }

  createEmptyGroceriesForm(): FormGroup {
    return this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        amountUsed: ['', [Validators.required, Validators.min(0.01), Validators.pattern('[0-9]+([\\.][0-9]{1,3})?$')]]
      },
      {updateOn: 'change'});
  }

  onAddGrocery(): void {
    if (!this.groceriesIsLoaded) {
      this.loadAllGroceries();
    }
    this.groceriesFormArray.push(this.createEmptyGroceriesForm());

    while (this.deletedGroceryFormCounter > 0) {
      this.groceriesForSelect.push(new Array(...this.allGroceries));
      this.deletedGroceryFormCounter--;

      for (const grocery of this.groceriesFormArray.value) {
        if (grocery.name !== null && grocery.name !== '') {
          this.groceriesForSelect[this.groceriesForSelect.length - 1]
            .splice(this.groceriesForSelect[this.groceriesForSelect.length - 1].indexOf(grocery.name), 1);
        }
      }
    }
  }

  deleteGrocery(index: number): void {

    const deletedGrocery = this.groceriesFormArray.value[index].name;

    this.groceriesFormArray.removeAt(index);
    this.lastChanged.splice(index, 1);

    if (deletedGrocery && deletedGrocery !== '') {
      this.groceriesForSelect.splice(index, 1);

      for (let i = 0; i < this.groceriesForSelect.length; i++) {
        if ((deletedGrocery && deletedGrocery !== '') && !this.groceriesForSelect[i].includes(deletedGrocery)) {
          this.groceriesForSelect[i].push(deletedGrocery);
        }
      }

      this.deletedGroceryFormCounter++;
    }
  }

  private loadAllGroceries(): void {
    this.pizzaService.allGroceries
      .subscribe((allGroceries: Grocery[]) => {
        allGroceries.forEach(grocery => {
          this.allGroceries.push(grocery.name);
        });
        this.allGroceries.forEach((grocery, index) => {
          this.groceriesForSelect[index] = new Array(...this.allGroceries);
        });
        this.groceriesIsLoaded = true;
      });
  }

  onChangeGrocery(controlIndex: number): void {

    const selectedGrocery = this.groceriesFormArray.value[controlIndex].name;

    for (let i = 0; i < this.groceriesForSelect.length; i++) {
      if (i !== controlIndex) {
        this.groceriesForSelect[i].splice(this.groceriesForSelect[i].indexOf(selectedGrocery), 1);
        if ((this.lastChanged[controlIndex] && this.lastChanged[controlIndex] !== '')
          && !this.groceriesForSelect[i].includes(this.lastChanged[controlIndex])) {
          this.groceriesForSelect[i].push(this.lastChanged[controlIndex]);
        }
      }
    }
    this.lastChanged[controlIndex] = selectedGrocery;
  }

  private changeFormToPizza(pizzaFormGroup: FormGroup): Pizza {

    const pizza = new class implements Pizza {
      assignedGroceries: AssignedGrocery[] = [];
      description: string;
      name: string;
      price: number;
      size: string;
    };
    pizza.name = this.nameFormControl.value;
    pizza.size = this.sizeFormControl.value;
    pizza.price = this.priceFormControl.value;
    pizza.description = this.descriptionFormControl.value;

    for (const formAssignedGrocery of this.groceriesFormArray.value) {
      const assignedGrocery: AssignedGrocery = new class implements AssignedGrocery {
        amountUsed: number = formAssignedGrocery.amountUsed;
        grocery: Grocery;
      };

      const grocery: Grocery = new class implements Grocery {
        id: number;
        name: string = formAssignedGrocery.name;
        price: number;
        unitsOnStock: number;
      };
      assignedGrocery.grocery = grocery;
      pizza.assignedGroceries.push(assignedGrocery);
    }

    return pizza;
  }

}
