import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Grocery, GroceryService} from '../groceries/grocery.service';

export interface Pizza {
  id?: number;
  name: string;
  size: string;
  price: number;
  description: string;
  assignedGroceries: AssignedGrocery[];
}

export interface AssignedGrocery {
  grocery: Grocery;
  amountUsed: number;
}

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  private pizzasUrl = 'http://localhost:8080/pizzas';

  constructor(private http: HttpClient,
              private groceryService: GroceryService) {
  }

  add(pizza: Pizza): Observable<object> {
    console.log(JSON.stringify(pizza));
    return this.http.post(
      this.pizzasUrl,
      pizza
    );
  }

  getAll(): Observable<object> {
    return this.http.get(
      this.pizzasUrl
    );
  }

  delete(id: number): Observable<object> {
    return this.http.delete(
      this.pizzasUrl + '/' + id
    );
  }

  getOne(id: number): Observable<object> {
    return this.http.get(
      this.pizzasUrl + '/' + id
    );
  }

  edit(id: number, pizza: Pizza): Observable<object> {
    return this.http.put(
      this.pizzasUrl + '/' + id,
      pizza
    );
  }

  get allGroceries(): Observable<object> {
    return this.groceryService.getAll();
  }

}

