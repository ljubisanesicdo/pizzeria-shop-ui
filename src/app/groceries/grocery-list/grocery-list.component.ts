import {Component, OnInit} from '@angular/core';
import {Grocery, GroceryService} from '../grocery.service';
import {ToastService} from '../../toast/toast.service';

@Component({
  selector: 'app-grocery-list',
  templateUrl: './grocery-list.component.html',
  styleUrls: ['./grocery-list.component.scss']
})
export class GroceryListComponent implements OnInit {

  groceries: Grocery[];

  constructor(private groceryService: GroceryService,
              private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.loadAllGroceries();
  }

  private loadAllGroceries(): void {
    this.groceryService.getAll()
      .subscribe((groceries: Grocery[]) => {
        this.groceries = groceries;
      });
  }

  onDelete(grocery: Grocery): void {
    if (confirm('Do you want do delete ' + grocery.name + '?')) {

      this.groceryService.delete(grocery.id)
        .subscribe(() => {
          this.showDeletedToast(grocery);
          this.removeGroceryFromArray(grocery);
        });
    }
  }

  private removeGroceryFromArray(grocery: Grocery): void {
    const index = this.groceries.indexOf(grocery);
    this.groceries.splice(index, 1);
  }

  private showDeletedToast(grocery: Grocery): void {
    this.toastService.show(
      'Grocery Deleted',
      'Name: ' + grocery.name + '<br/>' +
      'Price : ' + grocery.price + '<br/>' +
      'Units on Stock: ' + grocery.unitsOnStock,
      {classname: 'bg-danger text-white'}
    );
  }

}
