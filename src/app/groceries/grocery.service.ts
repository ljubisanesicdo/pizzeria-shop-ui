import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface Grocery {
  id?: number;
  name: string;
  price: number;
  unitsOnStock: number;

}

@Injectable({
  providedIn: 'root'
})
export class GroceryService {

  private groceriesUrl = 'http://localhost:8080/groceries';

  constructor(private http: HttpClient) {
  }

  add(grocery: Grocery): Observable<object> {
    return this.http.post(
      this.groceriesUrl,
      grocery
    );
  }

  getAll(): Observable<object> {
    return this.http.get(
      this.groceriesUrl
    );
  }

  delete(id: number): Observable<object> {
    return this.http.delete(
      this.groceriesUrl + '/' + id
    );
  }

  getOne(id: number): Observable<object> {
    return this.http.get(
      this.groceriesUrl + '/' + id
    );
  }

  edit(id: number, editedGrocery: Grocery): Observable<object> {
    return this.http.put(
      this.groceriesUrl + '/' + id,
      editedGrocery
    );
  }

}
