import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Grocery, GroceryService} from '../grocery.service';
import {ToastService} from '../../toast/toast.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-grocery-adding',
  templateUrl: './grocery-adding.component.html',
  styleUrls: ['./grocery-adding.component.scss']
})
export class GroceryAddingComponent implements OnInit {

  groceryFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private groceryService: GroceryService,
              private router: Router,
              private route: ActivatedRoute,
              public toastService: ToastService) {
  }

  ngOnInit(): void {
    this.createGroceryFormGroup();
  }

  private createGroceryFormGroup(): void {
    this.groceryFormGroup = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        price: ['', [Validators.required, Validators.min(0.1), Validators.pattern('[0-9]+([\\.][0-9]{1,2})?$')]],
        unitsOnStock: ['', [Validators.required, Validators.min(0.1), Validators.pattern('[0-9]+([\\.][0-9]{1,3})?$')]],
      },
      {updateOn: 'blur'});
  }

  get nameFormControl(): FormControl {
    return this.groceryFormGroup.get('name') as FormControl;
  }

  get priceFormControl(): FormControl {
    return this.groceryFormGroup.get('price') as FormControl;
  }

  get unitsOnStockFormControl(): FormControl {
    return this.groceryFormGroup.get('unitsOnStock') as FormControl;
  }

  onSubmit(): void {
    if (this.groceryFormGroup.valid) {
      this.groceryAdded(this.groceryFormGroup.value);
    } else {
      this.groceryFormGroup.markAllAsTouched();
    }
  }

  private groceryAdded(grocery: Grocery): void {
    this.groceryService.add(this.groceryFormGroup.value)
      .subscribe((returnedGrocery: Grocery) => {
        this.router.navigate(['../'], {relativeTo: this.route})
          .then(() => {
            this.showAddedToast(returnedGrocery);
          });
      });
  }

  private showAddedToast(returnedGrocery: Grocery): void {
    this.toastService.show(
      'Grocery Added',
      'Name: ' + returnedGrocery.name + '<br/>' +
      'Price: ' + returnedGrocery.price + '<br/>' +
      'Units on Stock: ' + returnedGrocery.unitsOnStock,
      {classname: 'bg-success text-light'}
    );
  }

}
