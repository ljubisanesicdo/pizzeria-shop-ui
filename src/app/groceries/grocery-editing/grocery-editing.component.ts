import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Grocery, GroceryService} from '../grocery.service';
import {ToastService} from '../../toast/toast.service';

@Component({
  selector: 'app-grocery-editing',
  templateUrl: './grocery-editing.component.html',
  styleUrls: ['./grocery-editing.component.scss']
})
export class GroceryEditingComponent implements OnInit {

  private id: number;
  groceryFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private groceryService: GroceryService,
              private router: Router,
              public toastService: ToastService) {
  }

  ngOnInit(): void {
    this.createGroceryFormGroup();

    this.id = this.route.snapshot.params.id;

    if (this.id) {
      this.groceryService.getOne(this.id)
        .subscribe((returnedGrocery: Grocery) => {
          this.setValuesToGroceryFormGroup(returnedGrocery);
        });
    }
  }

  private createGroceryFormGroup(): void {
    this.groceryFormGroup = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
        price: ['', [Validators.required, Validators.min(0.1), Validators.pattern('')]],
        unitsOnStock: ['', [Validators.required, Validators.min(0.1), Validators.pattern('')]]
      },
      {updateOn: 'blur'}
    );
  }

  private setValuesToGroceryFormGroup(returnedGrocery: Grocery): void {
    this.groceryFormGroup.setValue({
      name: returnedGrocery.name,
      price: returnedGrocery.price,
      unitsOnStock: returnedGrocery.unitsOnStock
    });
  }

  get nameFormControl(): FormControl {
    return this.groceryFormGroup.get('name') as FormControl;
  }

  get priceFormControl(): FormControl {
    return this.groceryFormGroup.get('price') as FormControl;
  }

  get unitsOnStockFormControl(): FormControl {
    return this.groceryFormGroup.get('unitsOnStock') as FormControl;
  }

  onSubmit(): void {
    if (this.groceryFormGroup.valid) {
      this.groceryEdited();
    } else {
      this.groceryFormGroup.markAllAsTouched();
    }
  }

  private groceryEdited(): void {
    this.groceryService.edit(this.id, this.groceryFormGroup.value)
      .subscribe((editedGrocery: Grocery) => {
        this.router.navigate(['../../'], {relativeTo: this.route})
          .then(() => {
            this.showEditedToast(editedGrocery);
          });
      });
  }

  private showEditedToast(editedGrocery: Grocery): void {
    this.toastService.show(
      'Grocery Edited',
      'Name: ' + editedGrocery.name + '<br/>' +
      'Price: ' + editedGrocery.price + '<br/>' +
      'Units on Stock: ' + editedGrocery.unitsOnStock,
      {classname: 'bg-warning text-white'}
    );
  }

}
