import {Component} from '@angular/core';
import {ToastService} from './toast/toast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pizzeria-shop-ui';

  constructor(public toastService: ToastService) {
  }
}
