import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main.component';
import {PizzasComponent} from './pizzas/pizzas.component';
import {PizzaAddingComponent} from './pizzas/pizza-adding/pizza-adding.component';
import {PizzaListComponent} from './pizzas/pizza-list/pizza-list.component';
import {PizzaEditingComponent} from './pizzas/pizza-editing/pizza-editing.component';
import {GroceriesComponent} from './groceries/groceries.component';
import {GroceryAddingComponent} from './groceries/grocery-adding/grocery-adding.component';
import {GroceryListComponent} from './groceries/grocery-list/grocery-list.component';
import {GroceryEditingComponent} from './groceries/grocery-editing/grocery-editing.component';

const routes: Routes = [
  {path: 'home', component: MainComponent},
  {
    path: 'pizzas', component: PizzasComponent,
    children: [
      {path: '', component: PizzaListComponent},
      {path: 'add', component: PizzaAddingComponent},
      {path: 'edit/:id', component: PizzaEditingComponent},
    ]
  },
  {
    path: 'groceries', component: GroceriesComponent,
    children: [
      {path: '', component: GroceryListComponent},
      {path: 'add', component: GroceryAddingComponent},
      {path: 'edit/:id', component: GroceryEditingComponent}
    ]
  },
  {path: '', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
