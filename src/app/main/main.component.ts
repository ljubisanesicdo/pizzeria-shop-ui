import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  images = [
    '../../assets/homePictures/pikado1.jpg',
    'assets/homePictures/pikado2.jpg',
    'assets/homePictures/pikado3.jpg',
    'assets/homePictures/pikado4.jpg'
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
