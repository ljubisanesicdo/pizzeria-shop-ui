import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {PizzasComponent} from './pizzas/pizzas.component';
import {MainComponent} from './main/main.component';
import {PizzaAddingComponent} from './pizzas/pizza-adding/pizza-adding.component';
import {PizzaService} from './pizzas/pizza.service';
import {PizzaListComponent} from './pizzas/pizza-list/pizza-list.component';
import {ToastComponent} from './toast/toast.component';
import {PizzaEditingComponent} from './pizzas/pizza-editing/pizza-editing.component';
import {GroceriesComponent} from './groceries/groceries.component';
import {GroceryAddingComponent} from './groceries/grocery-adding/grocery-adding.component';
import {GroceryListComponent} from './groceries/grocery-list/grocery-list.component';
import {GroceryEditingComponent} from './groceries/grocery-editing/grocery-editing.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PizzasComponent,
    MainComponent,
    PizzaAddingComponent,
    PizzaListComponent,
    ToastComponent,
    PizzaEditingComponent,
    GroceriesComponent,
    GroceryAddingComponent,
    GroceryListComponent,
    GroceryEditingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
